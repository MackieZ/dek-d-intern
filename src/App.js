import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Slider from './slider/slider';

class App extends Component {
  render() {
    return (
      <div className="App">
          <div className="Header"> Dek-D Intern </div>
          <div className="Sub-Header"> Slider Component </div>
          <Slider/>
      </div>
    );
  }
}

export default App;
