import React, { Component } from "react";
import "./slider.css";

class Slider extends Component {
    constructor() {
        super();

        this.slides = this.slides.bind(this);
        this.state = {
            imgUrl: [
                {
                    src:
                        "http://i44.tinypic.com/2iw3k3d.png"
                },
                {
                    src:
                        "http://www.lfvh.com/cn/images/background1.jpg"
                },
                {
                    src:
                        "http://backgroundcheckall.com/wp-content/uploads/2017/12/background-image-url.png"
                },
                {
                    src:
                        "http://definitivedesign.weebly.com/uploads/4/9/1/9/4919312/6548152_orig.jpg"
                },
                {
                    src:
                        "http://dodskypict.com/D/Awesome-Abstract-Wallpaper-On-Wallpaper-Hd-13-1680x1050.jpg"
                }
            ],
            selectedIndex: 1
        };
    }

    slides(index) {
        console.log(index);
       
        this.setState({
            selectedIndex: index 
        })
    }
    
    render() {
        return (
            <div>
                <div className="slideshow">
                    {this.state.imgUrl.map((url, index) => {
                        return <img src={url.src} className={`fade ${index === this.state.selectedIndex ? "selected" : ""}`}/>
                    })}

                    <div className="prev" onClick={() => this.slides(this.state.selectedIndex - 1 >= 0 ? this.state.selectedIndex - 1 : this.state.imgUrl.length - 1)}> &#10094; </div>
                    <div className="next" onClick={() => this.slides(this.state.selectedIndex + 1 < this.state.imgUrl.length ? this.state.selectedIndex + 1 : 0)}> &#10095; </div>
                </div>
                {this.state.imgUrl.map((url, index) => {
                    return <span className={`slidedot ${(index === this.state.selectedIndex) ? "slidedot-active" : ""}`} onClick={() => 
                    this.slides(index)}></span>
                })}
            </div>
        );
    }
}

export default Slider;
